# recalbox-themes

The recalbox emulationstation themes!

# License

The following themes are released under [Creative Commons Attribution-NonCommercial-NoDerivs 4.0 License CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/) ![](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png):
* recalbox
* recalbox-next
* recalbox-multi

The following themes are on an unknown license or yet to be decided:
* Blisslight
* eudora
* eudora-bigshot
* eudora-concise
* pixel

# Systems list to support:

Find below the systems list to support to obtain a theme compatible with recalboxOS.

| Systems | Folder's Name | Recalbox's Version |
| :--------------: | :--------------: | :--------------: |
| Amstrad CPC | amstradcpc | 4.0.0 |
| Atari 2600 | atari2600 | 4.0.0 |
| Atari 7800 | atari7800 | 4.0.0 |
| Atari ST | atarist | 4.0.0 |
| CaveStory | cavestory | 4.0.0 |
| Favoris | favorites | 4.0.0 |
| FDS (Family Computer Disk) | fds | 4.0.0 |
| FinalBurn Alpha | fba | 4.0.0 |
| FinalBurn Neo | fbneo | 4.0.0 |
| Game & Watch | gw | 4.0.0 |
| Game boy color | gbc | 4.0.0 |
| Game Gear | gamegear | 4.0.0 |
| Gameboy | gb | 4.0.0 |
| Gameboy  Advance | gba | 4.0.0 |
| Image Viewer | imageviewer | 4.0.0 |
| Lutro | lutro | 4.0.0 |
| Lynx | lynx | 4.0.0 |
| Mame | mame | 4.0.0 |
| Master System | mastersystem | 4.0.0 |
| Megadrive | megadrive | 4.0.0 |
| Moonlight | moonlight | 4.0.0 |
| MSX1 | msx1 | 4.0.0 |
| MSX2+ | msx2 | 4.0.0 |
| Neo Geo | neogeo | 4.0.0 |
| Neo Geo Pocket B&W | ngp | 4.0.0 |
| Neo Geo Pocket Color | ngpc | 4.0.0 |
| Nintendo | nes | 4.0.0 |
| Nintendo 64 | n64 | 4.0.0 |
| Odyssey 2 | odyssey2 | 4.0.0 |
| PC Engine | pcengine | 4.0.0 |
| PC Engine CD | pcenginecd | 4.0.0 |
| Playstation | psx | 4.0.0 |
| Doom | doom | 4.0.0 |
| Scumm VM | scummvm | 4.0.0 |
| Sega 32 X | sega32x | 4.0.0 |
| Sega CD | segacd | 4.0.0 |
| Sega SG 1000 | sg1000 | 4.0.0 |
| Super Nintendo | snes | 4.0.0 |
| Supergrafx | supergrafx | 4.0.0 |
| Vectrex | vectrex | 4.0.0 |
| Virtual Boy | virtualboy | 4.0.0 |
| Wonderswan B&W | wonderswan | 4.0.0 |
| Wonderswan Color | wonderswancolor | 4.0.0 |
| ZX Spectrum | zxspectrum | 4.0.0 |
| ZX81 | z81 | 4.0.0 |
|   |   |   |
| Apple II | apple2 | 4.1.0 |
| Colecovision | colecovision | 4.1.0 |
| Commodore 64 | c64 | 4.1.0 |
| DosBox | pc | 4.1.0 |
| Dreamcast | dreamcast | 4.1.0 |
| Gamecube | gc | 4.1.0 |
| Playstation portable | psp | 4.1.0 |
| Wii | wii | 4.1.0 |
|   |   |   |
| 3DO Interactive Multiplayer | 3do | 2018.02.09 |
| Amiga 600 | amiga1200 | 2018.02.09 |
| Amiga 1200 | amiga600 | 2018.02.09 |
| Nintendo DS | nds | 2018.02.09 |
| Sharp X68000 | x68000 | 2018.02.09 |
|   |   |   |
| Daphne | daphne | 2018.06.27 |
| Thomson TO8 | to8 | 2018.06.27 |
|   |   |   |
| Atari 800 | atari800 | 6.0 |
| Atari 5200 | atari5200 | 6.0 |
| Mattel Intellivision | intellivision | 6.0 |
| Atari Jaguar | jaguar | 6.0 |
| Satellaview | satellaview | 6.0 |
| Neo Geo CD | neogeocd | 6.0 |
| Bandai SuFami Turbo | sufami | 6.0 |
| Pokémon Mini | pokemini | 6.0 |
| Fairchild Channel F | channelf | 6.0 |
| Tangerine Oric | oric | 6.0 |
| MGT SAM Coupé | samcoupe | 6.0 |
| NEC PC-FX | pcfx | 6.0 |
| NEC PC-98 | pc98 | 6.0 |
|   |   |   |
| Amstrad GX4000 | gx4000 | 6.1 |
| Uzebox | uzebox | 6.1 |
| Apple IIGS | apple2gs | 6.1 |
| Spectravideo | spectravideo | 6.1 |
| Sharp X1 | x1 | 6.1 |
| Palm OS | palm | 6.1 |
| NEC PC-88 | pc88 | 6.1 |
| TIC-80 | tic80 | 6.1 |
| MSXturboR | msxturbor | 6.1 |
| Othello Multivision | multivision | 6.1 |
| Sega NAOMI | naomi | 6.1 |
| Sammy Atomiswave | atomiswave | 6.1 |
| Sega Saturn | saturn | 6.1 |
| Arcade | arcade | optional |
|   |   |   |
| Amiga CD32 | amigacd32 | 7.0 |
| Amiga CDTV | amigacdtv | 7.0 |
| All Games | auto-allgames | 7.0 |
| Last Played | auto-lastplayed | 7.0 |
| Multiplayer | auto-multiplayer | 7.0 |
| Nintendo 64DD | 64dd | 7.0 |
| OpenBOR | openbor | 7.0 |
| Ports | ports | 7.0 |
| Sega NAOMI GD-ROM System | naomigd | 7.0 |
| Solarus | solarus | 7.0 |
|   |   |   |
| Dragon | dragon | 7.X |
| Nintendo 3DS | 3ds | 7.X |
| Macintosh | macintosh | 7.X |
| PlayStation 2 | ps2 | 7.X |
| PlayStation 3 | ps3 | X.X |
| Wii U | wiiu | X.X |
| Kodi | kodi | optional |
